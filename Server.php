<?php
use Workerman\Worker;
header('Content-Type: text/html; charset=utf-8');
require_once __DIR__ . '/Workerman/Autoloader.php';
require_once __DIR__ . '/Workerman/vendor/autoload.php';
require_once 'Utils.php';

//(守护进程)方式运行
Worker::$daemonize = true;
$ws_worker = new Worker("websocket://0.0.0.0:3684");
// 启动4个进程对外提供服务
$ws_worker->count = 4;



//启动
try{
    $ws_worker->onWorkerStart = function($worker)
    {   
        $GLOBALS['arrconn'] = [];
        zlog("启动>>>");
    };
    
    $ws_worker->onConnect = function($connection)
    {   
        global $arrconn;
        zlog("用户连接>>>$connection->id");
        $arrconn[$connection->id] = $connection;
    };
    
    $ws_worker->onMessage = function($connection, $dataStr)
    {   
        global $arrconn;
        zlog("收到消息>>>",$dataStr);
        foreach ($arrconn as $key => $conn) {
            $conn->send($dataStr);
        }
    };
    
    $ws_worker->onClose = function($connection)
    {   
        global $arrconn;
        zlog("用户断开>>>$connection->id");
        unset($arrconn[$connection->id]);
    };
}catch(Exception $e){
    zlog($e);
}


// 运行worker
Worker::runAll();