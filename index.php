﻿<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>WebSocket</title>
    <!-- 控制宽度的自动适应 -->  
<style type="text/css">  
.comments {  
 width:100%;/*自动适应父布局宽度*/  
 overflow:auto;  
 word-break:break-all;  
/*在ie中解决断行问题(防止自动变为在一行显示，主要解决ie兼容问题，ie8中当设宽度为100%时，文本域类容超过一行时，  
当我们双击文本内容就会自动变为一行显示，所以只能用ie的专有断行属性“word-break或word-wrap”控制其断行)*/  
}  
</style>
<?php $host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''); ?>

    <script type="text/javascript">
let ws;
        function WebSocketTest() {
            if ("WebSocket" in window) {
                alert("您的浏览器支持 WebSocket!");
                var ip = '<?php echo $host; ?>'
                // 打开一个 web socket
                ws = new WebSocket("ws://"+ip+":3684");

                ws.onopen = function () {
                    // Web Socket 已连接上，使用 send() 方法发送数据
                    ws.send("用户连接上");
                    console.log("用户连接上...");
                    //alert("数据发送中...");
                };

                ws.onmessage = function (evt) {
                    var received_msg = evt.data;
                    console.log("数据已接收..."+received_msg);
                    cundiv(received_msg)
                    //alert("数据已接收...");
                };

                ws.onclose = function () {
                    // 关闭 websocket
                   alert("连接已关闭...");
                };

                
            }

            else {
                // 浏览器不支持 WebSocket
                alert("您的浏览器不支持 WebSocket!");
            }
        }
        function cundiv(str){
        var msgdiv = document.getElementsByClassName("msg");
        var div=document.createElement("div");
        div.class = 'msg';
        div.innerHTML = str;
        insertAfter(div,msgdiv[msgdiv.length - 1]);
    }
    function insertAfter(newElement, targetElement){
    var parent = targetElement.parentNode;
    if (parent.lastChild == targetElement) {
        // 如果最后的节点是目标元素，则直接添加。因为默认是最后
        parent.appendChild(newElement);
    } else {
        parent.insertBefore(newElement, targetElement.nextSibling);
        //如果不是，则插入在目标元素的下一个兄弟节点 的前面。也就是目标元素的后面
    }
}

    function onmsg(){
        var txt = document.getElementById("txtmsg").value;
        ws.send(txt);
        document.getElementById("txtmsg").value = '';
    }
    
    </script>
</head>
<body>
    <div id = 'onmsg'>
        <div class='msg'> WebSocket </div>
    </div>
    <textarea id='txtmsg' class="comments" style="height:expression((this.scrollHeight>150)?'150px':(this.scrollHeight+5)+'px');overflow:auto;"></textarea>
    <button onclick="onmsg()">sms</button>
    <div id="sse">
        <a href="javascript:WebSocketTest()">运行 WebSocket <?php echo $host;?> </a>
    </div>

</body>
</html>