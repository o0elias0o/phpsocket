<?php
/**
 * 工具类
 * User: Wally7
 * Date: 2017/3/27
 * Time: 11:49
 */

/**
 * zlog 日志函数,写入logfile.txt
 */
function zlog()
{   
    
    $arg_list = func_get_args();
    $s = "";
    foreach ($arg_list as $value){
        $s.= print_r($value,true);
    }
//  $s = print_r($arg_list,false);
    echo $s = date("Y-m-d H:i:s"). " " .  $s. "\r\n";
    file_put_contents('./logfile.txt', $s, FILE_APPEND | LOCK_EX);
}

/**
 * 生成UUID
 * @return string UUID
 */
function guid(){
    mt_srand((double)microtime()*10000);
    $charid = strtoupper(md5(uniqid(rand(), true)));
    $hyphen = chr(45);// "-"
    $uuid =
        substr($charid, 0, 8).$hyphen
        .substr($charid, 8, 4).$hyphen
        .substr($charid,12, 4).$hyphen
        .substr($charid,16, 4).$hyphen
        .substr($charid,20,12);
    return $uuid;
}